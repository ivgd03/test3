FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive
# software-properties-common python3-pip should be installed if you want python3.7
RUN apt-get update && apt-get install -y curl file vim git xz-utils unzip tzdata \
    software-properties-common python3-pip git-core \
    && ln -fs /usr/share/zoneinfo/Asia/Taipei /etc/localtime && dpkg-reconfigure -f noninteractive tzdata \
    && apt-get clean && rm -rf /var/cache/apt/* && rm -rf /var/lib/apt/lists/* && rm -rf /tmp/*

# install python3.7 and upgrade the related pip
RUN add-apt-repository ppa:deadsnakes/ppa -y \
    && apt-get update && apt-get install -y python3.7 \
    && python3.7 -m pip install --upgrade pip \
    && ln -fs /usr/bin/python3.7 /usr/bin/python3 \
    && pip3 install --upgrade pip \
    && pip3 install --upgrade requests bs4 python-telegram-bot \
    && apt-get clean && rm -rf /var/cache/apt/* && rm -rf /var/lib/apt/lists/* && rm -rf /tmp/*

# install node.js npm
RUN curl -sL https://deb.nodesource.com/setup_13.x | bash - && apt-get install -y nodejs

# install lib32z1
RUN apt-get update && apt-get install -y lib32z1 \
    && apt-get clean && rm -rf /var/cache/apt/* && rm -rf /var/lib/apt/lists/* && rm -rf /tmp/*

RUN mkdir /wksp
WORKDIR /wksp
ARG GO_VERSION="go1.13.8.linux-amd64"
ARG FFMPEG_VERSION="ffmpeg-4.2.1-amd64-static"
RUN mkdir -p /wksp/go && curl -O -sSL https://dl.google.com/go/${GO_VERSION}.tar.gz \
    && tar -xf ${GO_VERSION}.tar.gz && rm ${GO_VERSION}.tar.gz
RUN mkdir -p /wksp/avs && cd /wksp/avs && curl -o ffm.xz -sSL https://www.johnvansickle.com/ffmpeg/old-releases/${FFMPEG_VERSION}.tar.xz \
    && tar -xf ffm.xz \
    && mkdir -p ffmpeg/linux \
    && cp ${FFMPEG_VERSION}/ffmpeg ffmpeg/linux \
    && rm -rf ffm.xz ${FFMPEG_VERSION}


CMD ["/bin/bash"]
